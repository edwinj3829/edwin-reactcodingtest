import React, { Component } from 'react';
import IconImage from 'images/icon.png';
import SlotMachine from '../containers/SlotMachine';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <SlotMachine />
      </div>
    )
  }
}

export default App