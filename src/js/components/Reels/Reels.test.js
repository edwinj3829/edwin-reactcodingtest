import 'jsdom-global/register';
import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import Reels from './';
import Reel from '../Reel';

const shallowComponent = function(props) {
    return shallow(<Reels {...props} />);
}
const mountComponent = function(props, context) {
    return mount(<Reels {...props} />, { context });
}

describe('components/Reels', () => {
    it('renders three <Reel> elements', () => {
        const wrapper = shallowComponent({ reels: [
            {
                options: ['red'],
                selectedOption: 'red'
            },
            {
                options: ['red'],
                selectedOption: 'red'
            },
            {
                options: ['red'],
                selectedOption: 'red'
            }
        ]});
        expect(wrapper.find(Reel)).to.have.length(3);
    });
});