import React from 'react';
import PropTypes from 'prop-types';
import Reel from '../Reel';
import './style.styl';

function Reels(props) {
    return (
        <div id="reelsContainer">
            {props.reels.map((reel, i) => (
                <Reel 
                    key={`reel-${i}`} 
                    reelId={`reel-${i}`}
                    colors={reel.options}
                    selectedColor={reel.selectedOption}
                />
            ))}
        </div>
    )
}

Reels.propTypes = {
    reels: PropTypes.arrayOf(PropTypes.shape({
        options: PropTypes.arrayOf(PropTypes.string),
        selectedOption: PropTypes.string
    })).isRequired
}

export default Reels;
