import React from 'react';
import PropTypes from 'prop-types';
import SlotBlock from '../SlotBlock';

function Reel(props) {
    const { colors, reelId, selectedColor } = props;
    return (
        <div style={{ display: 'inline-block', float: 'left' }}>
            <div>
                {colors.map(color => (
                    <SlotBlock
                        key={`${reelId}--${color}`}
                        color={color}
                        isActive={selectedColor === color}
                    />
                ))}
            </div>
        </div>
    )
};

Reel.propTypes = {
    colors: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedColor: PropTypes.string.isRequired
}

export default Reel;
