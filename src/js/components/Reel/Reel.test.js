import 'jsdom-global/register';
import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import Reel from './';
import SlotBlock from '../SlotBlock';

const shallowComponent = function(props) {
    return shallow(<Reel {...props} />);
}
const mountComponent = function(props, context) {
    return mount(<Reel {...props} />, { context });
}

describe('components/Reel', () => {
    it('renders a <SlotBlock> element', () => {
        const wrapper = shallowComponent({ colors: ['red'], selectedColor: 'red'});
        expect(wrapper.find(SlotBlock)).to.have.length(1);
    });
});