import React from 'react';
import PropTypes from 'prop-types';
import tinycolor from 'tinycolor2';
import './style.styl';

function SlotBlock(props, context) {
    const { color, isActive } = props;
    const { hasFinished, hasWon } = context;
    return (
        <div 
            className="slotBlock"
            style={{
                backgroundColor: color, 
                borderColor: `${ isActive ? 'black' : 'white' }`,
                color: tinycolor(color).isDark() ? 'white' : 'black'
            }}
        >
            { isActive && hasFinished && <i className={`fa fa-${hasWon ? 'check' : 'times'}`}></i> }
        </div>
    );
}

SlotBlock.contextTypes = {
  hasFinished: PropTypes.bool,
  hasWon: PropTypes.bool
};

SlotBlock.propTypes = {
    color: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired
}

export default SlotBlock;
