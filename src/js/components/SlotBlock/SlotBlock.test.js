import 'jsdom-global/register';
import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import SlotBlock from './';

const shallowComponent = function(props) {
    return shallow(<SlotBlock {...props} />);
}
const mountComponent = function(props, context) {
    return mount(<SlotBlock {...props} />, { context });
}

describe('components/SlotBlock', () => {
    it('renders a <div> element', () => {
        const wrapper = shallowComponent({ color: 'red', isActive: false });
        expect(wrapper.find('.slotBlock')).to.have.length(1);
    });
    it('renders an <i> element if it is selected', () => {
        const wrapper = mountComponent({ color: 'red', isActive: true }, { hasFinished: true, hasWon: true });
        expect(wrapper.find('i')).to.have.length(1);
    })
});