import 'jsdom-global/register';
import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';

import SlotMachine from './';
import Reels from '../../components/Reels';

const shallowComponent = function(props) {
    return shallow(<SlotMachine {...props} />);
}
const mountComponent = function(props, context) {
    return mount(<SlotMachine {...props} />, { context });
}

describe('containers/SlotMachine', () => {
    it('renders a <Reels> element', () => {
        const wrapper = shallowComponent();
        expect(wrapper.find(Reels)).to.have.length(1);
    });
    it('renders a <p> element', () => {
        const wrapper = shallowComponent();
        expect(wrapper.find('p')).to.have.length(1);
    });
    it('renders a <button> element', () => {
        const wrapper = shallowComponent();
        expect(wrapper.find('button')).to.have.length(1);
    });
    describe('on click', () => {
        const wrapper = shallowComponent();
        wrapper.find('button').simulate('click');
        it('changes the text of the message field', () => {
            expect(wrapper.find('p').text()).to.equal('Message: Spinning...');
        })
        it('disables the button', () => {
            expect(wrapper.find('button').props().disabled).to.equal(true)
        });
    });
    describe('on winning spin', () => {
        const wrapper = shallowComponent();
        wrapper.setState({
            score: 'Yay! You won!'
        })
        it('changes the text of the message field', () => {
            expect(wrapper.find('p').text()).to.equal('Message: Yay! You won!');
        })
    });
    describe('on losing spin', () => {
        const wrapper = shallowComponent();
        wrapper.setState({
            score: 'Oh no! Better luck next time!'
        })
        it('changes the text of the message field', () => {
            expect(wrapper.find('p').text()).to.equal('Message: Oh no! Better luck next time!');
        })
    })
});