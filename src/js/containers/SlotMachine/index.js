import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Reels from '../../components/Reels';

class SlotMachine extends Component {
    constructor() {
        super();
        this.state = {
            spinning: false,
            reels: [
                {
                    options: [
                        'red',
                        'blue',
                        'yellow',
                        'green'
                    ],
                    selectedOption: 'red'
                },
                {
                    options: [
                        'red',
                        'blue',
                        'yellow',
                        'green'
                    ],
                    selectedOption: 'red'
                },
                {
                    options: [
                        'red',
                        'blue',
                        'yellow',
                        'green'
                    ],
                    selectedOption: 'red'
                }
            ]
        };
    }
    getChildContext() {
        return { hasFinished: !!this.state.score, hasWon: this.slotReelsMatched(this.state.reels.map(reel => reel.selectedOption)) };
    }
    start = () => {
        this.setState({
            spinning: true,
            score: null
        });
        this.changeReel();
    }
    changeReel = () => {
        const reels = this.state.reels;
        const startTime = new Date().getTime();
        reels.map((reel, i) => {
            const changeSelectedOption = setInterval(() => {
                const currentSelectedOption = reel.selectedOption;
                const currentOptionIndex = reel.options.indexOf(currentSelectedOption);
                const newCurrentOption = currentOptionIndex === reel.options.length - 1 ? reel.options[0] : reel.options[currentOptionIndex + 1];
                reels[i].selectedOption = newCurrentOption;
                this.setState({
                    reels
                })
                if ((startTime + (3000 + (1000 * i))) <= new Date().getTime()) {
                    clearInterval(changeSelectedOption);
                    if (i === 2 && !this.state.score) {
                        this.setState({
                            spinning: false,
                            score: this.slotReelsMatched(reels.map(reel => reel.selectedOption)) ? 'Yay! You won!' : 'Oh no! Better luck next time!'
                        })
                    }
                }
            }, Math.floor(Math.random() * (200 - 150)) + 150)
        });
    }
    slotReelsMatched = (reelResult) => {
        return reelResult.every(reel => reel === reelResult[0]);
    }
    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <Reels 
                                reels={this.state.reels}
                                isSpinning={this.state.spinning}
                            />
                        </div>
                    </div>
                </div>
                <p>Message: { this.state.spinning ? 'Spinning...' : this.state.score ? this.state.score : 'Press the button below to spin.'}</p>
                <br />
                <button 
                    className="btn btn-primary"
                    disabled={this.state.spinning} 
                    onClick={this.start}
                >
                    Spin!
                </button>
            </div>
        );
    }
};

SlotMachine.childContextTypes = {
  hasFinished: PropTypes.bool,
  hasWon: PropTypes.bool
};

export default SlotMachine;
